"""Methods to aggragate data returned from game queries."""

import cherrypy
import memcache
import decimal
import requests
import heapq
from collections import defaultdict
from gamestradamus import cli, timeutils, units
import hydro.atoms
import plumbing.auth


#-------------------------------------------------------------------------------
class Search(object):

    #---------------------------------------------------------------------------
    def __init__(self, default_url=None, index_url_map=None):
        self.default_url = 'http://127.0.0.1:8088'
        if default_url is not None:
            self.default_url = default_url

        self.index_url_map = {}
        if index_url_map is not None:
            self.index_url_map = index_url_map

    #---------------------------------------------------------------------------
    def relatives(self, steam_app_id, hours_played, count):
        """Return the closest relatives for a given steam app id and hours played."""

        url = self.index_url_map.get(steam_app_id, self.default_url)
        response = requests.get(url, params={
            'steam_app_id': steam_app_id, 'hours_played': hours_played, 'count': count
        })
        return response.json

#-------------------------------------------------------------------------------
def find_relatives(steam_id_64, count, sandbox):
    """Match the closest relatives to a query account.

    returns: [
        (weight, steam_id_64),
        (weight, steam_id_64),
        etc.
    ]
    sorted DESC by weight
    """
    steam_id_64 = int(steam_id_64)

    # Configure a new search interface for querying games.
    config = cli.get_configuration()
    default_url = config.get('global', {}).get('crystalball.default_url', None)

    search = Search(default_url=default_url)

    user_account = sandbox.UserAccount(**{
        'openid': (u'http://steamcommunity.com/openid/id/%s' % steam_id_64)
    })
    if user_account is None:
        return []

    zero = decimal.Decimal('0.00')
    results = sandbox.recall(
        (units.UserAccountGame << units.Game) & units.Game,
        lambda x, y, z: x.user_account_id == user_account.id
    )

    # Drop any games played that have less than 1 hour playtime, or are explicitly being ignored.
    results = [
        x for x in results if (
            (x[0].steam_hours_played or 0) + (x[0].custom_hours_played or 0)
        ) > 1 and not x[0].ignore
    ]
    results.sort(**{
        'key': lambda x: (
            (x[0].steam_hours_played or 0) + (x[0].custom_hours_played or 0)
        ),
        'reverse': True
    })

    # Tally all the accounts that match each game.
    match_factor_min = 1.0
    match_factor_max = 4.0
    match_factor_range = match_factor_max - match_factor_min

    game_factor_min = 1.0
    game_factor_max = 4.0
    game_factor_range = game_factor_max - game_factor_min 

    relative_lookup = defaultdict(lambda: defaultdict(float))
    for j, game in enumerate(results):

        game_factor = game_factor_max - ((game_factor_range * j) / len(results))
        game_relatives = search.relatives(
            game[1].app_id if not game[1].canonical_game_id else game[2].app_id,
            (game[0].steam_hours_played or 0) + (game[0].custom_hours_played or 0),
            400
        )
        for i, relative in enumerate(game_relatives):
            match_factor = match_factor_max - ((match_factor_range * i) / len(game_relatives))
            relative_lookup[relative['steam_id_64']]['weight'] += (match_factor + game_factor)
            relative_lookup[relative['steam_id_64']]['count'] += 1

    closest_relatives = []
    for steam_id_64_, values in relative_lookup.iteritems():
        heapq.heappush(closest_relatives, (values['weight'], values['count'], steam_id_64_))

    closest_relatives = heapq.nlargest(count + 1, closest_relatives)
    closest_relatives = [x for x in closest_relatives if x[2] != steam_id_64]
    return closest_relatives

#-------------------------------------------------------------------------------
def update_family_tree(steam_id_64, count, store=None):
    # Setup storage access.
    clean_up_store = False
    if not store:
        store = cli.storage_manager()
        clean_up_store = True
    sandbox = store.new_sandbox()

    user_account = sandbox.UserAccount(**{
        'openid': 'http://steamcommunity.com/openid/id/%s' % steam_id_64
    })
    if user_account is None:
        return

    user_attrs = user_account.UserAccountAttributes()
    if user_attrs is None:
        user_attrs = units.UserAccountAttributes(**{
            'user_account_id': user_account.id
        })
        sandbox.memorize(user_attrs)

    # Figure out a new set of relatives and pull their records from the DB.
    relatives = find_relatives(steam_id_64, count, sandbox)
    relatives = [
        (x[0], x[1], sandbox.UserAccount(**{
            'openid': 'http://steamcommunity.com/openid/id/%s' % x[2]
        })) for x in relatives
    ]
    relatives = [x for x in relatives if x[2] is not None]

    # Forget the old relatives 
    for relative in sandbox.xrecall(
        units.UserAccountRelative,
        lambda x: x.user_account_id == user_account.id
    ):
        relative.forget()

    # Memorize the new relatives.
    for relative in relatives:
        new_relative = units.UserAccountRelative(**{
            'user_account_id': user_account.id,
            'relative_id': relative[2].id,
            'similar_count': relative[1],
            'match_score': '%.2f' % relative[0]
        })
        sandbox.memorize(new_relative)

    # Update the user_attrs for relatives
    if user_attrs:
        user_attrs.relatives_last_updated = timeutils.utcnow()

    sandbox.flush_all()
    if clean_up_store:
        store.shutdown()

#-------------------------------------------------------------------------------
def make_predictions(steam_id_64, count, sandbox):
    """Return a set of game recommendations for a consumer.

    TODO: Update this code to take into account the (canonical_game)cg attribute on Game entities.
    """

    mapping = defaultdict(list)
    user_account = sandbox.UserAccount(**{
        'openid': 'http://steamcommunity.com/openid/id/%s' % steam_id_64
    })
    if user_account is None:
        return []

    # Pull all the games for all of the relatives.
    relatives = sandbox.xrecall(
        units.UserAccountRelative,
        lambda x: x.user_account_id == user_account.id
    )
    for relative in relatives:
        rows, cols = sandbox.store.db.fetch("""
            SELECT "UserAccountGame".game_id AS game_id,
                "Game".canonical_game_id AS canonical_game_id,
                "UserAccountGame".steam_hours_played as hours_played
            FROM "UserAccountGame"
            INNER JOIN "Game"
                ON "Game".id = "UserAccountGame".game_id
            LEFT JOIN "UserAccountAttributes"
                ON "UserAccountAttributes".user_account_id = "UserAccountGame".user_account_id
            WHERE "UserAccountGame".steam_hours_played > 0.0
                AND "UserAccountGame".user_account_id = %s
                AND "Game".for_sale = true
                AND "UserAccountAttributes".private_profile_since IS NULL
                AND "UserAccountAttributes".data_usage_opt_out IS NULL
            """ % (relative.relative_id, )
        )

        for row in rows:
            if row[1]:
                mapping[row[1]].append(row[2])
            else:
                mapping[row[0]].append(row[2])

    # Remove anything that the person already owns.
    games = user_account.UserAccountGame()
    for game in games:
        mapping.pop(game.game_id, None)

        # If the game is an alias, remove the canonical version too.
        canonical_game_id = game.Game().canonical_game_id
        if canonical_game_id:
            mapping.pop(canonical_game_id, None)

    # Remove anything that is not for sale.
    games = sandbox.xrecall(
        units.Game,
        lambda x: x.for_sale == False
    )
    for game in games:
        mapping.pop(game.id, None)

    # Tally everything up and make some value predictions.
    best_predictions = []
    for game_id in mapping.keys():
        hours = mapping[game_id]
        if len(hours) < 25:
            continue

        # Calculate the initial mean. 
        mean = (sum(hours) / len(hours))

        # Calculate the standard deviations.
        sigma = decimal.Decimal('0')
        for hour in hours:
            sigma += (hour - mean) ** 2
        sigma /= len(hours)
        sigma = sigma ** decimal.Decimal('0.5')

        lower = mean - (sigma * 3)
        upper = mean + (sigma * 3)

        # Trim any samples that are 3 standard deviations from the mean.
        hours = [x for x in hours if x >= lower and x <= upper]

        if len(hours) < 25:
            continue

        heapq.heappush(
            best_predictions,
            (sum(hours) / len(hours), game_id)
        )

    return heapq.nlargest(count, best_predictions)

#-------------------------------------------------------------------------------
def prophesy(steam_id_64, count, store=None):
    steam_id_64 = int(steam_id_64)

    # Setup storage access.
    clean_up_store = False
    if not store:
        store = cli.storage_manager()
        clean_up_store = True

    sandbox = store.new_sandbox()

    user_account = sandbox.UserAccount(**{
        'openid': 'http://steamcommunity.com/openid/id/%s' % steam_id_64
    })
    if user_account is None:
        return

    user_attrs = user_account.UserAccountAttributes()
    if user_attrs is None:
        user_attrs = units.UserAccountAttributes(**{
            'user_account_id': user_account.id
        })
        sandbox.memorize(user_attrs)

    # Figure out a new set of predictions.
    predictions = make_predictions(steam_id_64, count, sandbox)
    for prediction in user_account.UserAccountPrediction():
        prediction.forget()

    # Memorize the new predictions.
    for prediction in predictions:
        new_prediction = units.UserAccountPrediction(**{
            'user_account_id': user_account.id,
            'game_id': prediction[1],
            'hours_played': prediction[0]
        })
        sandbox.memorize(new_prediction)

    # Update the user_attrs for relatives
    if user_attrs:
        user_attrs.predictions_last_updated = timeutils.utcnow()
        user_attrs.predictions_updating = False

    sandbox.flush_all()
    if clean_up_store:
        store.shutdown()

#-------------------------------------------------------------------------------
if __name__ == '__main__':
    #cherrypy.Application.memcache = memcache.Client(
    #    ['127.0.0.1:11211']
    #)

    #update_family_tree(76561197992171346, 200)
    #update_family_tree(76561197993100157, 200)

    #prophesy(76561197992171346, 100)
    prophesy(76561197993100157, 100)

    #print recommendations(76561197992171346, 100)
    #print recommendations(76561197993100157, 100)
    # Configure a new search interface for querying games.
    #search = Search(
        #default_url='http://127.0.0.1:8080/',
    #)
    #print search.relatives(10190, 40.5, 1000)

